package com.example.logisticahumanitaria.DataBase;
import android.provider.BaseColumns;

public final class DefinirTabla {

        public DefinirTabla(){}
        public static abstract class Estados implements BaseColumns {
            public static final String TABLE_NAME = "estados";
            public static final String COLUMN_NAME_NOMBRE = "nombre";
            public static final String COLUMN_NAME_STATUS = "status";

        }
}
