package com.example.logisticahumanitaria.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class ProyectoEstado {
    Context context;
    EstadoDbHelper mDbHelper;
    SQLiteDatabase db;
    String[] columnsToRead = new String[]{
            DefinirTabla.Estados._ID,
            DefinirTabla.Estados.COLUMN_NAME_NOMBRE,
            DefinirTabla.Estados.COLUMN_NAME_STATUS
    };

    public ProyectoEstado(Context context) {
        this.context = context;
        mDbHelper = new EstadoDbHelper(this.context);
    }

    public void openDatabase() {
        db = mDbHelper.getWritableDatabase();
    }

    public long insertEstado(Estados e) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estados.COLUMN_NAME_NOMBRE, e.getNombre());
        values.put(DefinirTabla.Estados.COLUMN_NAME_STATUS, e.getStatus());
        return db.insert(DefinirTabla.Estados.TABLE_NAME, null, values);
//regresa el id insertado
    }

    public long updateContacto(Estados e, long id) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estados.COLUMN_NAME_NOMBRE, e.getNombre());
        values.put(DefinirTabla.Estados.COLUMN_NAME_STATUS, e.getStatus());
        Log.d("values", values.toString()); //numero de filas afectadas
        return db.update(DefinirTabla.Estados.TABLE_NAME, values,
                DefinirTabla.Estados._ID + " = " + id, null);
    }

    public int deleteContacto(long id) {
        return db.delete(DefinirTabla.Estados.TABLE_NAME,
                DefinirTabla.Estados._ID + "=?",
                new String[]{String.valueOf(id)});
    }
    private Estados readContacto(Cursor cursor){
        Estados c = new Estados();
        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setStatus(cursor.getInt(2));
        return c;
    }
    public Estados getContacto(long id){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Estados.TABLE_NAME,
                columnsToRead,
                DefinirTabla.Estados._ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null
        );
        c.moveToFirst();
        Estados estado = readContacto(c);
        c.close();
        return estado;
    }
    public ArrayList<Estados> allContactos(){
        Cursor cursor = db.query(DefinirTabla.Estados.TABLE_NAME,
                columnsToRead, null, null, null, null, null);
        ArrayList<Estados> contactos = new ArrayList<Estados>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Estados c = readContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }
    public void close(){
        mDbHelper.close();
    }
}
